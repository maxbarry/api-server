# API Server package
Creates the folder structure and admin interface for rapidly deploying new Mongo backed API servers.

Each instance uses the Falcon framework to efficiently handle GET, POST, PUT and DELETE requests.
Ideally paired with a frontend framework, not included in this repository.