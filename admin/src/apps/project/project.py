import sys
import os
import shutil
from django.conf import settings
from components.updateproject import run as run_update
from components.newproject import run as run_new
from components.deleteproject import run as run_delete


class Project(object):
    """docstring for Project"""
    def __init__(self, command=False, name=False, request=False):
        super(Project, self).__init__()
        self.conf = {
            "CUR_DIR": os.path.dirname(os.path.realpath(__file__)),
            "PROJ_REL": settings.INSTANCE_PATH,
            "REPO_URL": "https://maxbarry@bitbucket.org/maxbarry/falcon-api-server.git"
        }

        # Check if two command line args have been passed in
        unfound_arg_msg = "Must provide both type of command (e.g. \"new\") and the name of the project"
        if __name__ == '__main__':
            try:
                command = sys.argv[1]
                name = sys.argv[2]
            except IndexError:
                self.raise_error(unfound_arg_msg)
        elif not command and not name or len(name) == 0:
            self.raise_error(unfound_arg_msg)

        # Set self.command to the command and self.name to the project name
        self.conf["name"] = name
        self.conf["command"] = command

        # Store request for progress updates
        self.conf["request"] = request

        # Create path to our new project directory (unless an array has been provided)
        print isinstance(self.conf["name"], basestring)
        if isinstance(self.conf["name"], basestring):
            print 'Setting proj dir'
            self.conf["PROJ_DIR"] = os.path.join(self.conf["CUR_DIR"], self.conf["PROJ_REL"], self.conf["name"])

    # Reset the build to initial state in case of error in install process
    def reset_build(self):
        print "Removing partial project\n"
        shutil.rmtree(self.conf["PROJ_DIR"])
        return

    # Raise an exception and optionally reset the build
    def raise_error(self, msg, reset=False):
        if reset:
            self.reset_build()
        raise Exception(msg)

    def update_status(self, prog, msg):
        print "Updating status"
        if self.conf["request"]:
            self.conf["request"].session["progress"] = (prog, msg)
            self.conf["request"].session.save()
        return

    def check_action_result(self, return_status):
        if isinstance(return_status, str):
            self.raise_error(return_status)
        else:
            return

    # COMMANDS
    # Update a declared instance of Falcon app. Invoke with project.py update [PROJECT_NAME]
    def update(self):
        print 'Updating Falcon instance for project: %s\n' % self.conf["name"]
        ret = run_update(self)
        self.check_action_result(ret)
        return

    # Create a new project. Invoke with project.py new [PROJECT_NAME]
    def new(self):
        print 'Starting new project: %s\n' % self.conf["name"]

        # Set update state
        self.update_status(10, "Starting a new project...")

        ret = run_new(self)
        self.check_action_result(ret)
        return

    def delete(self):
        print 'Deleting projects: %s\n' % ", ".join(self.conf["name"])

        ret = run_delete(self)
        self.check_action_result(ret)
        return

if __name__ == "__main__":
    proj = Project()

    # Binding commands to their respective methods
    command_dict = {
        "update": proj.update,
        "new": proj.new
    }

    # Binding arguments to their respective methods
    command_dict[proj.conf["command"]]()
