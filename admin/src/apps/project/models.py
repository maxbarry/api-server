from django.db import models
from django.utils.translation import ugettext as _
from libs.utils.abstract_models import BaseFields


class Projects(BaseFields):
    name = models.CharField(max_length=256)
    project_name = models.CharField(max_length=256)
    project_location = models.CharField(max_length=512)

    class Meta:
        verbose_name = _('Project')
        verbose_name_plural = _('Projects')

    def __unicode__(self):
        return self.name
