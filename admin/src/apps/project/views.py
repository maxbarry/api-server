import os
from models import Projects
from django.views.generic import ListView, View
from django.template.defaultfilters import slugify
from braces.views import JSONResponseMixin, CsrfExemptMixin
from project import Project as ProjectHandler


class Project(ListView):
    model = Projects
    template_name = "home.html"
    http_method_names = [u"get"]
    context_object_name = "projects"

    def get_context_data(self, **kwargs):
        context = super(Project, self).get_context_data(**kwargs)
        self.request.session["progress"] = (0, "None")
        return context

    def get_queryset(self):
        return Projects.objects.filter(active=True)


class CreateProject(CsrfExemptMixin, JSONResponseMixin, View):
    http_method_names = [u'post', u'options']

    def post(self, request, *args, **kwargs):

        # Set progress to 0
        self.request.session["progress"] = (0, "Creating project...")

        # Form params
        project_name = request.POST["name"]
        # Slug the name so you can create easier folder structures
        slugged_name = slugify(project_name)

#        try:
        # Instantiate a new project object
        proj = ProjectHandler("new", slugged_name, request)
        # Call new method
        proj.new()

        msg = "Project successfully built"
        status_msg = "success"

        # Create the new record for it
        # TODO: Fill out all the attributes
        p = Projects(name=project_name, project_name=slugged_name, project_location=os.path.abspath(proj.conf["PROJ_DIR"]))
        p.save()

#        except Exception, e:
            # Errors in project creation raise exceptions
            # Catch them here and pass them to the frontend.
#            msg = e.message
#            status_msg = "fail"

        context_dict = {
            "message": msg,
            "status": status_msg,
            "project_name": project_name
        }

        # Update progress to complete
        self.request.session["progress"] = (100, "Project successfully created")
        self.request.session.save()

        return self.render_json_response(context_dict)


class DeleteProject(CsrfExemptMixin, JSONResponseMixin, View):
    http_method_names = [u'post', u'options']

    def post(self, request, *args, **kwargs):

        projects_to_delete = request.POST.getlist("projects")

        try:
            # Instantiate a new project object
            proj = ProjectHandler("delete", projects_to_delete, request)
            # Call new method
            proj.delete()

            msg = "Project(s) successfully deleted"
            status_msg = "success"

        except Exception, e:
            # Errors in project deletion raise exceptions
            # Catch them here and pass them to the frontend.
            msg = e.message
            status_msg = "fail"

        context_dict = {
            "message": msg,
            "status": status_msg,
            "deleted": projects_to_delete,
        }

        return self.render_json_response(context_dict)


class UpdateProgress(JSONResponseMixin, View):
    http_method_names = [u'get']

    def get(self, request, *args, **kwargs):
        context_dict = {}
        context_dict["progress"], context_dict["msg"] = request.session["progress"]

        return self.render_json_response(context_dict)
