from git import Repo


def update_instance(proj):
    try:
        # Create python-git object of our project repository
        repo = Repo(proj.conf["PROJ_DIR"])
        # Pull latest changes in git
        repo.git.pull()
    except Exception, e:
        return "Git failed to update the Falcon App: %s" % e
    return


commands = [
    update_instance
]


def run(proj):
    return_status = True
    for command in commands:
        return_status = command(proj)
        if isinstance(return_status, str):
            break
    return return_status
