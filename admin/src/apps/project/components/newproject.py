import os
import string
import subprocess
from git import Repo
from django.conf import settings

# COMMANDS


# Create symlink of uwsgi skel for this instance
def symlink_uwsgi(proj):
    print "Making uwsgi symlink"
    try:
        # Create symlink
        subprocess.call(["ln", "-s", settings.UWSGI_SKEL, os.path.abspath(os.path.join(settings.UWSGI_VASSAL_PATH, "%s.ini" % proj.conf["name"]))])
    except Exception, e:
        return str(e.message)

    subprocess.call(["sudo", "service", "nginx", "reload"])
    return


# Create NGINX conf
def create_nginx(proj):
    proj.update_status(80, "Creating NGINX conf")
    conf_name = "%s.conf" % proj.conf["name"]
    nginx_conf_path = os.path.abspath(os.path.join(settings.NGINX_PATH, conf_name))
    if os.path.isfile(nginx_conf_path):
        return "NGINX conf already exists for project name: %n" % ()

    # Use NGINX conf template from settings
    template = string.Template(settings.NGINX_TEMPLATE)
    # Substitute in the project name
    result = template.substitute({'instance': proj.conf["name"]})
    with open(nginx_conf_path, 'a+b') as conf_file:
        conf_file.write(result)
        conf_file.close()

#    try:
#        subprocess.call(["sudo", "service", "nginx", "reload"])
#        update_msg = "Conf created and NGINX reloaded"
#    except Exception, e:
#        print e
#        update_msg = "Conf created but NGINX could not be reloaded"

    # Set update state post Nginx conf creation
#    proj.update_status(90, update_msg)
    return


# Create project folder
def create_project(proj):
    proj_dir = proj.conf["PROJ_DIR"]
    print 'Project location: %s\n' % proj_dir
    if os.path.exists(proj_dir):
        return "Project folder already exists"
    else:
        os.mkdir(proj_dir)

        # Set update state
        proj.update_status(30, "Project folder created")
    return


# Git pull down Falcon app
def clone_falcon(proj):
    print 'Cloning instance of Falcon App...\n'
    proj_dir = proj.conf["PROJ_DIR"]
    try:
        # Set update state
        proj.update_status(40, "Cloning instance of Api Server...")

        # Clone an instance of Falcon app
        Repo.clone_from(proj.conf["REPO_URL"], proj_dir)

        # Set update state
        proj.update_status(60, "Api Server cloned")

        # Create python-git object of our cloned directory
        repo = Repo(proj_dir)

        # Set update state
        proj.update_status(75, "Switching to live branch")

        # Checkout live branch
        repo.git.checkout("live")

    except Exception, e:
        return "Git failed to clone instance of Falcon App: %s" % e
    print 'Falcon App cloned successfully.\n'
    return


commands = [
    create_project,
    clone_falcon,
    create_nginx,
    symlink_uwsgi,
]


def run(proj):
    return_status = True
    for command in commands:
        return_status = command(proj)
        if isinstance(return_status, str):
            break
    return return_status
