import os
import shutil
from apps.project.models import Projects


def remove_project_folder(proj):

    def delete_folder(path):
        print path
        shutil.rmtree(path, True)
        return

    def return_path(name):
        return os.path.join(proj.conf["CUR_DIR"], proj.conf["PROJ_REL"], name)

    if isinstance(proj.conf["name"], basestring):
        delete_folder(return_path(proj.conf["name"]))
    else:
        for project in proj.conf["name"]:
            delete_folder(return_path(project))
    return


def delete_record(proj):

    def del_record(name):
        obj = Projects.objects.get(project_name=name)
        obj.active = False
        obj.save()
        return

    if isinstance(proj.conf["name"], basestring):
        del_record(proj.conf["name"])
    else:
        for project in proj.conf["name"]:
            del_record(project)
    return


commands = [
    remove_project_folder,
    delete_record,
]


def run(proj):
    return_status = True
    for command in commands:
        return_status = command(proj)
        if isinstance(return_status, str):
            break
