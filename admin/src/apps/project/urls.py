from django.conf.urls import patterns, url
from views import Project, CreateProject, UpdateProgress, DeleteProject

urlpatterns = patterns('',
    url(r'^$', Project.as_view(), name="Project"),
    url(r'^project/create/$', CreateProject.as_view(), name="CreateProject"),
    url(r'^project/create/progress/$', UpdateProgress.as_view(), name="CurrentProgress"),
    url(r'^project/delete/$', DeleteProject.as_view(), name="DeleteProject")
)
