$(function() {

    var $del_copy = $("#project-multicontrols h3.delete");

    function create_deletion_array($form, options){
        // Reset style changes from unsuccessful deletions on $del_copy
        $del_copy.removeAttr('style');
        // Reset any previous deleted work
        $form.find('input').remove();
        // Find all the checked projects, clone their checkbox values, hide them and append them to the form
        var checked_elements = $project_workspace.find("input:checked").clone().hide();
        $form.append(checked_elements);
        // Once appended jquery.form can then serialize them.
        return;
    }

    function form_success(response, status, xhr, $form){
        console.log(response);
        // Style the text of the responsive message depending on status
        cop_col = response.status === "fail" ? err_col : suc_col;
        $del_copy.css("color", cop_col);
        // Add the status message to section
        $del_copy.text(response.message);

        // Remove the list item that has previously been deleted
        var proj_del;
        for (var i = response.deleted.length - 1; i >= 0; i--) {
            proj_del = response.deleted[i];
            $('.' + proj_del).remove();
        }

        return;
    }

    var opts = {
        beforeSerialize: create_deletion_array,
        success: form_success,
        clearForm: true,
    };

    $('form#delete-project').ajaxForm(opts);

});