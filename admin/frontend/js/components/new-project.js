$(function() {

    var POLL_INTERVAL,
        SUBMITTING = false,
        $progress_bar = $('#start-project .progress-bar span'),
        $progress_copy = $('#start-project h3');

    // Repeatedly polls the server for an update on create project process status
    function poll_progress(arr, $form, options){

        var prog;

        SUBMITTING = true;

        // Reset progress bar
        $progress_bar.removeAttr('style');

        POLL_INTERVAL = setInterval(function(){
            $.getJSON("/project/create/progress/", function(resp){
                // If the progress has updated since the last check...
                if (resp.progress != prog && SUBMITTING){
                    // Update the progress value
                    prog = resp.progress;
                    // Update the progress bar copy
                    $progress_copy.text(resp.msg);
                    // Animate the increase in width for progress bar
                    $progress_bar.animate({"width": prog.toString() + "%"}, 150);
                }
            });
        }, 10);
        return;
    }

    // Append the successfully created project to the list of projects
    function update_project_workspace(name){
        $project_workspace.append(
            '<dt>' + name + '</dt>'
        );
        return;
    }

    function form_success(response, status, xhr, $form){
        SUBMITTING = false;
        clearInterval(POLL_INTERVAL);
        if (response.status === "success"){
            // Update progress bar to reflect complete state
            $progress_bar.css({
                "background-color": suc_col,
                "width": "100%"
            });
            $progress_copy.text(response.message);
            // Update project listings with new project
            update_project_workspace(response.project_name);
        }
        else{
            // Error in creation process. Display the error message returned.
            console.log(response);
            $progress_copy.text(response.message);
            // Style progress bar red to highlight error
            $progress_bar.css({
                "background-color": err_col,
                "width": "100%"
            });
        }
        return;
    }

    // If server returns an error, stop the setInterval
    function form_error(response, status, xhr, $form){
        clearInterval(POLL_INTERVAL);
        return;
    }

    var opts = {
        //beforeSubmit: poll_progress,
        success: form_success,
        error: form_error,
        clearForm: true,
    };

    $('form#new-project').ajaxForm(opts);

});
